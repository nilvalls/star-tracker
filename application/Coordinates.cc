#include "Coordinates.h"

using namespace std;

// Constructor
Coordinates::Coordinates(double iRA, double iDec, unsigned int iFocus){
	dec		= iDec;
	ra		= iRA;
	focus	= iFocus;
	Normalize();
}

// Copy constructor
Coordinates::Coordinates(const Coordinates& iCoordinates){
	dec		= iCoordinates.GetDec();
	ra		= iCoordinates.GetRA();
	focus	= iCoordinates.GetFocus();
	Normalize();
}

// A more explicit constructor, note coordinates are always stored as floating-point values (degrees, hours) or simply % respetively..
Coordinates::Coordinates(int iRA_hours, int iRA_minutes, double iRA_seconds, int iDec_hours, int iDec_minutes, double iDec_seconds, unsigned int iFocus){
	dec = GetDecimalRA(iRA_hours, iRA_minutes, iRA_seconds);
	ra	= GetDecimalDec(iRA_hours, iRA_minutes, iRA_seconds);
	focus = iFocus;
	Normalize();
}


// Destructor
Coordinates::~Coordinates(){ }

// Simple arithmetic operators
Coordinates Coordinates::operator+ (Coordinates iElement) {
	return Coordinates(dec + iElement.GetDec(), ra + iElement.GetRA(), focus);
}

Coordinates Coordinates::operator- (Coordinates iElement) {
	return Coordinates(dec - iElement.GetDec(), ra - iElement.GetRA(), focus);
}

// Normalize Coordinates: RA in hours [0,24), declination in degrees [-90,90]
void Coordinates::Normalize(){

	// Constrain Dec to (-180,180]
	dec = fmod(dec + 180, 360);
	if (dec < 0){ dec += 360; }
	dec -= 180;

	// Constrain Dec to [-90,90], adjust RA when appropriate
	if(fabs(dec) > 90){
		dec = (dec > 0 ? (180 - dec) : (-180-dec));	
		ra += 12;
	}

	// Constrain RA to [0,24)
	ra = fmod(ra, 24);
	if (ra < 0){ ra += 24; }

	// A few tweaks for aesthetics
	if(dec == 0.0){ dec = 0; }
	if(ra == 0.0){ ra = 0; }
}

// Standard getters
double Coordinates::GetRA() const { return ra; }
double Coordinates::GetDec() const { return dec; }
unsigned int Coordinates::GetFocus() const { return focus; }

// Get a formated string of the RA [00h 00' 00"]
string Coordinates::GetRAText() const {
	stringstream result;
	char buff[10];
	sprintf(buff, "%02d", GetRAhours());
	result << buff << "h ";
	sprintf(buff, "%02d", GetRAminutes());
	result << buff << "\' ";
	sprintf(buff, "%04.1f", GetRAseconds());
	result << buff << "\"";
	return result.str();
}

// Get a formated string of the declination [00° 00' 00"]
string Coordinates::GetDecText() const {
	stringstream result;
	char buff[10];
	sprintf(buff, "%02d", GetDecDegrees());
	result << buff << "\u00b0 ";
	sprintf(buff, "%02d", GetDecMinutes());
	result << buff << "\' ";
	sprintf(buff, "%04.1f", GetDecSeconds());
	result << buff << "\"";
	return result.str();
}

// Get a formated string of the focus [00%]
string Coordinates::GetFocusText() const {
	stringstream result;
	char buff[10];
	sprintf(buff, "%02d%s", GetFocus(), "%%");
	result << buff;
	return result.str();
}

// RA getters for each of its integer parts (except for the seconds)
unsigned int Coordinates::GetRAhours() const { return floor(ra); }
unsigned int Coordinates::GetRAminutes() const { return floor(((int)(ra*60)) % 60 ); }
double Coordinates::GetRAseconds() const { 
	double minutes = ra*60;
	return floor(((int)(minutes*60)) % 60 );
}

// Declination getters for each of its integer parts (except for the seconds)
unsigned int Coordinates::GetDecDegrees() const { return floor(dec); }
unsigned int Coordinates::GetDecMinutes() const { return floor(((int)(dec*60)) % 60 ); }
double Coordinates::GetDecSeconds() const { 
	double minutes = dec*60;
	return floor(((int)(minutes*60)) % 60 );
}

// Conversion from explicit coordinates to floating-point
double Coordinates::GetDecimalRA(int iHours, int iMinutes, double iSeconds){ return (iHours + (iMinutes/60.0) + (iSeconds/3600.0)); }
double Coordinates::GetDecimalDec(int iDegrees, int iMinutes, double iSeconds){ return (iDegrees + (iMinutes/60.0) + (iSeconds/3600.0)); }

// Get a formatted string of the RA and declination (debugging purposes)
string Coordinates::GetString(){
	stringstream result; result.str("");
	result << "(RA,Dec): (" << setprecision(5) << ra << "," << setprecision(5) << dec << ")"; 
	return result.str();
}

// Print out summary string
void Coordinates::Print(){ cout << GetString() << endl; }
