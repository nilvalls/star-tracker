/*
 * Quick-and-dirty way to map characters to their key code and vice-versa.
 * Should think of something better...
 */

#ifndef _KeyCodes_H
#define _KeyCodes_H

namespace kc{

	// Give the character, get the key code
	inline int C2I(char iChar){ 
		switch(iChar){
			case 'a': return 97; break;
			case 'b': return 98; break;
			case 'c': return 99; break;
			case 'd': return 100; break;
			case 'e': return 101; break;
			case 'f': return 102; break;
			case 'g': return 103; break;
			case 'h': return 104; break;
			case 'i': return 105; break;
			case 'j': return 106; break;
			case 'k': return 107; break;
			case 'l': return 108; break;
			case 'm': return 109; break;
			case 'n': return 110; break;
			case 'o': return 111; break;
			case 'p': return 112; break;
			case 'q': return 113; break;
			case 'r': return 114; break;
			case 's': return 115; break;
			case 't': return 116; break;
			case 'u': return 117; break;
			case 'v': return 118; break;
			case 'w': return 119; break;
			case 'x': return 120; break;
			case 'y': return 121; break;
			case 'z': return 122; break;
			case 'A': return 65; break;
			case 'B': return 66; break;
			case 'C': return 67; break;
			case 'D': return 68; break;
			case 'E': return 69; break;
			case 'F': return 70; break;
			case 'G': return 71; break;
			case 'H': return 72; break;
			case 'I': return 73; break;
			case 'J': return 74; break;
			case 'K': return 75; break;
			case 'L': return 76; break;
			case 'M': return 77; break;
			case 'N': return 78; break;
			case 'O': return 79; break;
			case 'P': return 80; break;
			case 'Q': return 81; break;
			case 'R': return 82; break;
			case 'S': return 83; break;
			case 'T': return 84; break;
			case 'U': return 85; break;
			case 'V': return 86; break;
			case 'W': return 87; break;
			case 'X': return 88; break;
			case 'Y': return 89; break;
			case 'Z': return 90; break;
			default: return -1; break;
		}
	}

	// Give the key code, get the character
	inline char I2C(const int iNum){ 
		switch(iNum){
			case 97: return 'a'; break;
			case 98: return 'b'; break;
			case 99: return 'c'; break;
			case 100: return 'd'; break;
			case 101: return 'e'; break;
			case 102: return 'f'; break;
			case 103: return 'g'; break;
			case 104: return 'h'; break;
			case 105: return 'i'; break;
			case 106: return 'j'; break;
			case 107: return 'k'; break;
			case 108: return 'l'; break;
			case 109: return 'm'; break;
			case 110: return 'n'; break;
			case 111: return 'o'; break;
			case 112: return 'p'; break;
			case 113: return 'q'; break;
			case 114: return 'r'; break;
			case 115: return 's'; break;
			case 116: return 't'; break;
			case 117: return 'u'; break;
			case 118: return 'v'; break;
			case 119: return 'w'; break;
			case 120: return 'x'; break;
			case 121: return 'y'; break;
			case 122: return 'z'; break;
			case 65: return 'A'; break;
			case 66: return 'B'; break;
			case 67: return 'C'; break;
			case 68: return 'D'; break;
			case 69: return 'E'; break;
			case 70: return 'F'; break;
			case 71: return 'G'; break;
			case 72: return 'H'; break;
			case 73: return 'I'; break;
			case 74: return 'J'; break;
			case 75: return 'K'; break;
			case 76: return 'L'; break;
			case 77: return 'M'; break;
			case 78: return 'N'; break;
			case 79: return 'O'; break;
			case 80: return 'P'; break;
			case 81: return 'Q'; break;
			case 82: return 'R'; break;
			case 83: return 'S'; break;
			case 84: return 'T'; break;
			case 85: return 'U'; break;
			case 86: return 'V'; break;
			case 87: return 'W'; break;
			case 88: return 'X'; break;
			case 89: return 'Y'; break;
			case 90: return 'Z'; break;
			default: return '?'; break;
		}
	}

}


#endif

