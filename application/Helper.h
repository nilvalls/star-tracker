/*
 * Helper file, for convenience when implementing includes or functions that could be used
 * in many places.
 */

#ifndef _Helper_H
#define _Helper_H

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <time.h>
//#include <armadillo>

#ifndef ColoredOutput_h
#define ColoredOutput_h

#define GRAY    "\033[0;30m"
#define RED     "\033[0;31m"
#define GREEN   "\033[0;32m"
#define BLUE    "\033[0;34m"
#define ORANGE  "\033[0;33m"
#define PURPLE  "\033[0;35m"
#define CYAN    "\033[0;36m"
#define WHITE   "\033[1;37m"
#define BGRAY    "\033[0;40m"
#define BRED     "\033[0;41m"
#define BGREEN   "\033[0;42m"
#define BBLUE    "\033[0;44m"
#define BORANGE  "\033[0;43m"
#define BPURPLE  "\033[0;45m"
#define BCYAN    "\033[0;46m"
#define BWHITE   "\033[1;47m"
#define NOCOLOR "\e[0m"

#endif


	// Different types of printouts: error, warning, info, debug...
	inline void FatalError(std::string const iMessage){			std::cerr << RED	<< ">>> [FATAL ERROR] " << iMessage << NOCOLOR << std::endl; exit(1); }
	inline void Error(std::string const iMessage){				std::cerr << RED	<< ">>> [ERROR]       " << iMessage << NOCOLOR << std::endl; }
	inline void Info(std::string const iMessage){				std::cout << GREEN	<< ">>> [INFO]        " << iMessage << NOCOLOR << std::endl; }
	inline void Warning(std::string const iMessage){			std::cout << ORANGE	<< ">>> [WARNING]     " << iMessage << NOCOLOR << std::endl; }
	inline void Debug(std::string const iMessage){ if(false){	std::cout << CYAN	<< ">>> [DEBUG]       " << iMessage << NOCOLOR << std::endl; }}
	inline void SimulationWarning(){
		if(false){
			std::cout	<< "\n"
					<< "\t******************************\n"
					<< "\t***    SIMULATION MODE     ***\n"
					<< "\t******************************\n"
					<< "\n" << std::endl;
		}
	}


#endif
