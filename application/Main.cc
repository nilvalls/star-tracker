#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>  /* only for sleep() */
#include <stdio.h>
#include <time.h>
#include "Main.h"
#include "Button.h"
#include "Axis.h"
#include "Coordinates.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

using namespace std;

// Main function
int main(){
	Info("Starting StarTracker...");

	// Initialize Ncurses UI
	InitNUI();

	int wwidth, wheight;
	getmaxyx(stdscr, wheight, wwidth);
	int lastKey = 0;

	// Data
	Coordinates* position	= new Coordinates(0,0,0);
	Coordinates* target		= new Coordinates(*position);
	Coordinates* speed		= new Coordinates(0,0,0);

	// Axes with controls
	WINDOW *controls_win = newwin(22-3, wwidth, 1, 0);
	Axis ra		(AxisType::RA,		controls_win, position, target, speed);
	Axis dec	(AxisType::DEC,		controls_win, position, target, speed);
	Axis focus	(AxisType::FOCUS,	controls_win, position, target, speed);

	// Menu
	ITEM **my_items;
	MENU *my_menu;
	int n_choices, i;
	ITEM *cur_item;
	WINDOW *my_menu_win;
	n_choices = ARRAY_SIZE(choices);
	my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));
	for(i = 0; i < n_choices; ++i){ my_items[i] = new_item(choices[i], ""); }
	my_items[n_choices] = (ITEM *)NULL;
	my_menu = new_menu((ITEM **)my_items);
	my_menu_win = newwin(wheight-22-2, wwidth, 22, 1);
	keypad(my_menu_win, TRUE);
	set_menu_win(my_menu, my_menu_win);
	set_menu_mark(my_menu, "");
	post_menu(my_menu);
	wrefresh(my_menu_win);

	// Main loop
	stringstream lastKeySS;
	while( lastKey != 81){ // 81 is 'Q'

		// Get keyboard input (if any)
		bool kbhit = KBhit(&lastKey);

		// Refresh clock
		attron(COLOR_PAIR(2));
		mvwprintw(stdscr,0, 2, currentDateTime().c_str());
		attroff(COLOR_PAIR(2));

		// Refresh axes
		ra.Refresh(&lastKey);
		dec.Refresh(&lastKey);
		focus.Refresh(&lastKey);
		wrefresh(controls_win);
		
		// Refresh info on keyboard input (debugging purposes)
		lastKeySS.str("");
		lastKeySS << "Current key: " << lastKey << "    ";
		mvprintw(wheight-1,0, lastKeySS.str().c_str());
		lastKeySS.str("");
		if(lastKey != -1){
			lastKeySS << "Last key: " << lastKey;
			mvprintw(wheight-1,25, lastKeySS.str().c_str());
		}

		// If key is hit, refresh menu if applicable
		if(kbhit){
			switch(lastKey){
				case KEY_DOWN:	menu_driver(my_menu, REQ_DOWN_ITEM);	break;
				case KEY_UP:	menu_driver(my_menu, REQ_UP_ITEM);		break;
			}
			wrefresh(my_menu_win);
		}

		// Refresh main window
		refresh();

		// Sleep some
		usleep(100000);
	}

	// Deallocate menu items
	free_item(my_items[0]);
	free_item(my_items[1]);
	free_menu(my_menu);

	// End
	endwin();

	Info("Terminating... Good bye!\n");
	return 0;
}
