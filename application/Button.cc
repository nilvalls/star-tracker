#include "Button.h"

using namespace std;

// Constructor
Button::Button(WINDOW* iWindow, const int iRow, const int iCol, const string iText, const int iKeyPress){
	text = iText;	// Button text
	row	= iRow;		// Placement
	col = iCol;
	window = iWindow;
	key_press = iKeyPress;
	key_hold = key_press - 32; // Upper case key codes are 32 numbers less than lower case ones
	hold = false;

	mvwprintw(window, iRow, iCol, iText.c_str()); // Draw button
	wrefresh(window);
}

// Destructor
Button::~Button(){

}

// Turn this button on, i.e. change color, etc. If the pressed key is the hold key for this button, DO NOT turn it off right after
void Button::TurnOn(const int* iKey){

	wattron(window, COLOR_PAIR(3)); // Default color
	wattron(window, A_REVERSE); // Negative
	mvwprintw(window, row, col, text.c_str()); // Print button with text
	wattroff(window, A_REVERSE);
	wattroff(window, COLOR_PAIR(3));
	hold = false;
	if(iKey != NULL){ hold = (*iKey == key_hold); } // Hold if pressed key is hold key for this button

}

// Reset button to off state
void Button::TurnOff(){
	wattron(window, COLOR_PAIR(3));
	mvwprintw(window, row, col, text.c_str());
	hold = false;
}

// Hold button, i.e. do not turn it off immediatel
void Button::Hold(){
	TurnOn();
	hold = true;
}

// Check button to see if it needs to be turned off
void Button::Refresh(){ if(!hold){ TurnOff(); } }

// Getter methods for retrieving information
int Button::GetKeyPress(){ return key_press; }
int Button::GetKeyHold(){ return key_hold; }
bool Button::IsHolding(){ return hold; }
bool Button::IsOn(){ return false; }
