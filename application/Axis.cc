#include "Axis.h"
#include <sstream>

using namespace std;

// Constructor
Axis::Axis(AxisType::AxisType iType, WINDOW* iWindow, Coordinates* iPosition, Coordinates* iTarget, Coordinates* iSpeed){

		window = iWindow;	// Window where to draw this axis' info/controls
		type = iType;		// Which of the three axes is this?

		// Each of the 3 sets of coordinates characterizing this axis
		position 	= iPosition;
		target		= iTarget;
		speed		= iSpeed;

		// Set up text and keys associated with buttons
		char key_mminus, key_minus, key_halt, key_plus, key_pplus;
		string name;
		switch(type){
			case AxisType::RA:		name = "Right Ascension";	row = 2; 
									key_mminus = 'r';	key_minus = 't';	key_halt = 'y';	key_plus = 'u';	key_pplus = 'i';
			break;
			case AxisType::DEC:		name = "Declination";		row = 8;
									key_mminus = 'f';	key_minus = 'g';	key_halt = 'h';	key_plus = 'j';	key_pplus = 'k';
			break;
			case AxisType::FOCUS: 	name = "Focus";				row = 14;
									key_mminus = 'c';	key_minus = 'v';	key_halt = 'b';	key_plus = 'n';	key_pplus = 'm';
			break;
		}

		// Draw the name of the axis
		name += ":";
		wattron(window, A_BOLD);
		wattron(window, COLOR_PAIR(1));
		mvwprintw(window, row, 5, name.c_str());
		wattroff(window, COLOR_PAIR(1));
		wattroff(window, A_BOLD);

		// Setup buttons
		int buttonXoffset = 40;
		mminus	= new Button(window,	row+1, buttonXoffset + 0,	" -- [" + string(&key_mminus,1) + "] ",		kc::C2I(key_mminus));
		minus	= new Button(window,	row+1, buttonXoffset + 9,	"  - [" + string(&key_minus,1) + "] ",		kc::C2I(key_minus));
		halt	= new Button(window,	row+1, buttonXoffset + 19,	"  [" + string(&key_halt,1) + "]  ",	kc::C2I(key_halt));
		plus	= new Button(window,	row+1, buttonXoffset + 28,	" [" + string(&key_plus,1) + "] +  ",	kc::C2I(key_plus));
		pplus	= new Button(window,	row+1, buttonXoffset + 37,	" [" + string(&key_pplus,1) + "] ++ ",	kc::C2I(key_pplus));

		// Draw buttons
		wattron(window, COLOR_PAIR(4));
		mvwprintw(window, row+1, 7, "Current... ");
		mvwprintw(window, row+2, 7, "Target.... ");
		mvwprintw(window, row+3, 7, "Speed..... ");
		wattroff(window, COLOR_PAIR(4));

		// Initialize buttons
		mminus	->TurnOff();
		minus	->TurnOff();
		halt	->Hold();
		plus	->TurnOff();
		pplus	->TurnOff();
}

// Destructor
Axis::~Axis(){
		delete mminus;
		delete minus;
		delete halt;
		delete plus;
		delete pplus;
}

void Axis::Refresh(const int* iKey){

	// Refresh data
	stringstream positionSS, targetSS, speedSS;
	positionSS.str(""); targetSS.str(""); speedSS.str("");

	switch(type){
		case AxisType::RA:
			positionSS	<< position->GetRAText();
			targetSS	<< target->GetRAText();
			speedSS		<< speed->GetRAText() << " /min";
		break;
		case AxisType::DEC:
			positionSS	<< position->GetDecText();
			targetSS	<< target->GetDecText();
			speedSS		<< speed->GetDecText() << " /min";
		break;
		case AxisType::FOCUS:
			positionSS	<< position->GetFocusText();
			targetSS	<< target->GetFocusText();
			speedSS		<< speed->GetFocusText() << " /min";
		break;
	}

	wattron(window, COLOR_PAIR(4));
	mvwprintw(window, row+1,	18, positionSS.str().c_str());
	mvwprintw(window, row+2,	18, targetSS.str().c_str());
	mvwprintw(window, row+3,	18, speedSS.str().c_str());
	wattroff(window, COLOR_PAIR(4));


	// Refresh buttons
	if(*iKey == halt->GetKeyPress() || *iKey == halt->GetKeyHold()){
		mminus	->TurnOff();
		minus	->TurnOff();
		halt	->Hold();
		plus	->TurnOff();
		pplus	->TurnOff();
	}else if(*iKey == plus->GetKeyPress() || *iKey == plus->GetKeyHold()){
		mminus	->TurnOff();
		minus	->TurnOff();
		halt	->TurnOff();
		plus	->TurnOn(iKey);
		pplus	->TurnOff();
	}else if(*iKey == pplus->GetKeyPress() || *iKey == pplus->GetKeyHold()){
		mminus	->TurnOff();
		minus	->TurnOff();
		halt	->TurnOff();
		plus	->TurnOff();
		pplus	->TurnOn(iKey);
	}else if(*iKey == minus->GetKeyPress() || *iKey == minus->GetKeyHold()){
		mminus	->TurnOff();
		minus	->TurnOn(iKey);
		halt	->TurnOff();
		plus	->TurnOff();
		pplus	->TurnOff();
	}else if(*iKey == mminus->GetKeyPress() || *iKey == mminus->GetKeyHold()){
		mminus	->TurnOn(iKey);
		minus	->TurnOff();
		halt	->TurnOff();
		plus	->TurnOff();
		pplus	->TurnOff();
	}else if(!mminus->IsHolding() && !minus->IsHolding() && !plus->IsHolding() && !pplus->IsHolding()){
		mminus	->TurnOff();
		minus	->TurnOff();
		halt	->Hold();
		plus	->TurnOff();
		pplus	->TurnOff();
	}else{
		mminus	->Refresh();
		minus	->Refresh();
		halt	->Refresh();
		plus	->Refresh();
		pplus	->Refresh();
	}

}
