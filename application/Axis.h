/*
 * Axis class to support each of the 3 axes of rotation/motors, with corresponding
 * buttons, logic, etc.
 */

#ifndef _Axis_H
#define _Axis_H

#include <string>
#include <ncurses.h>

#include "Button.h"
#include "KeyCodes.h"
#include "Coordinates.h"

// Enum for each of the three axes
namespace AxisType{ enum AxisType{RA, DEC, FOCUS}; }

class Axis{

	// === Functions and Operators === //
	public:
		Axis(AxisType::AxisType, WINDOW* iWindow, Coordinates*, Coordinates*, Coordinates*);
		~Axis();

		void Refresh(const int*);

	protected:

	private:


	// === Variables === //
	public:

	protected:

	private:
		WINDOW* window;
		unsigned int row;

		// Control buttons
		Button* mminus;
		Button* minus;
		Button* halt;
		Button* plus;
		Button* pplus;

		Coordinates* position;
		Coordinates* target;
		Coordinates* speed;

		AxisType::AxisType type;

};


#endif
