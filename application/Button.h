/*
 * Button class for ncurses interface, specifically for the motor controls.
 */

#ifndef _Button_H
#define _Button_H

#include <ncurses.h>
#include <menu.h>
#include <string>

class Button{

	// === Functions and Operators === //
	public:
		Button(WINDOW*, const int, const int, const std::string, const int);
		~Button();

		void TurnOn(const int* iKey = NULL);
		void TurnOff();

		void Hold();
		void Refresh();
		bool IsHolding();
		bool IsOn();
		int GetKeyPress();
		int GetKeyHold();

	protected:

	private:


	// === Variables === //
	public:

	protected:

	private:
		WINDOW* window;		// Ncurses window where this button will be put
		std::string text;	// Text of the button
		unsigned int row;	// Row number within the window where button will start
		unsigned int col;	// Column number within the window where button will start
		int key_press;		// Key code that will treat this button as a push-down button (released immediately)
		int key_hold;		// Key code that will hold the button pressed
		bool hold;			// Is this button on hold?

};

#endif
