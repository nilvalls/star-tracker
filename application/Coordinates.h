/*
 * Coordinates class contains RA, declination, and image focus values
 * and supporting functions
 */

#ifndef _Coordinates_H
#define _Coordinates_H

#include "Helper.h"
#include <math.h>


class Coordinates{

	// === Functions and Operators === //
	public:
		Coordinates(double, double, unsigned int iFocus=0);
		Coordinates(const Coordinates&);
		Coordinates(int, int, double, int, int, double, unsigned int iFocus=0);
		virtual ~Coordinates();
		Coordinates operator + (Coordinates);
		Coordinates operator - (Coordinates);

		double GetDec() const;
		double GetRA() const;
		unsigned int GetFocus() const;

		std::string GetDecText() const;
		std::string GetRAText() const;
		std::string GetFocusText() const;

		unsigned int GetRAhours() const;
		unsigned int GetRAminutes() const;
		double GetRAseconds() const;

		unsigned int GetDecDegrees() const;
		unsigned int GetDecMinutes() const;
		double GetDecSeconds() const;

		std::string GetString();
		void Print();

	protected:
		double GetDecimalRA(int, int, double);
		double GetDecimalDec(int, int, double);

	private:
		void Normalize();


	// === Variables === //
	public:

	protected:
		double dec;			// Declination in floating-point degrees
		double ra;			// Right ascension in floating-point hours
		unsigned int focus; // Focus in %

	private:

};


#endif
