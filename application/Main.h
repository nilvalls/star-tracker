/*
 * Main file header file
 */

#include <string>
#include <ncurses.h>
#include <menu.h>


// Menu choices (to be developed further)
const char *choices[] = {
	"Calibrate                         ",
	"Compensate for Earth's rotation   ",
	"Slew to object                    ",
	"Slew to coordinates               ",
};

// Initialize NUI (pseudo GUI, Ncurses UI)
void InitNUI(){

	initscr();	// Main window
	cbreak();	// Huh?
	noecho();	// Don't echo key strokes
	keypad(stdscr, TRUE); // Enable keypad
	nodelay(stdscr,TRUE); // Don't wait for key strokes
	curs_set(0); // Hide cursor

	// Some color pairs
	start_color();
	init_pair(1, COLOR_YELLOW, COLOR_BLACK); // Main text
	init_pair(2, COLOR_YELLOW, COLOR_RED);  // Horizontal separators and their text
	init_pair(3, COLOR_YELLOW, COLOR_RED); // Buttons
	init_pair(4, COLOR_BLUE, COLOR_BLACK); // Buttons

	int wwidth, wheight;
	getmaxyx(stdscr, wheight, wwidth);
	int lastKey = 0;

	// Header and footer
	attron(COLOR_PAIR(2));
	for(unsigned int i=0; i<wwidth; i++){
		move(0,i); addch(' ');
		move(20,i); addch(' ');
		move(wheight-2,i); addch(' ');
	}
	std::string header = "Nil's StarTracker v0.1";
	mvprintw(0, (wwidth-header.length())/2, header.c_str());
	std::string footer = " Q: Quit  S: Stop  L: (Un)Lock"; 
	mvprintw(wheight-2,0, footer.c_str()); 
	attroff(COLOR_PAIR(2));
	refresh();

}


// Get current date/time, format is [HH:MM:SS Dow DD/MMM/YYYY UTC]
const std::string currentDateTime() {
	time_t     now		= time(0);
	//struct tm  tstruct	= *localtime(&now);
	tm* gmtm			= gmtime(&now);
	char       buf[80];
	// Visit http://www.cplusplus.com/reference/clibrary/ctime/strftime/
	strftime(buf, sizeof(buf), "%X - %a %d/%h/%Y UTC", gmtm);
	return buf;
}

// Has the keyboard been hit?
int KBhit(int* iLastKey){
	int ch = getch(); // Get key
	*iLastKey = ch; // The iLastKey pointer is shared among different classes 

	if (ch != ERR) { // ERR == -1, no key is pressed
		//ungetch(ch);
		return 1;
	}else{
		return 0;
	}
}
