#ifndef ButtonPanel_H
#define ButtonPanel_H

#include <ncurses.h>
#include <menu.h>
#include <string>
#include <map>
#include "Button.h"

class ButtonPanel{

	// === Functions and Operators === //
	public:
		ButtonPanel(WINDOW*, const int*);
		virtual ~ButtonPanel();

		virtual void Update(const int*) = 0;

	protected:

	private:


	// === Variables === //
	public:
		void Add(const std::string, const unsigned int, const unsigned int, const std::string, const int);

	protected:
		int* lastKey;
		WINDOW* window;
		//std::map<const std::string, Button*> buttons;
		std::map<const int, Button*> buttons;

	private:

};


#endif
