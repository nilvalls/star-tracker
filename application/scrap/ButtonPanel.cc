#include "ButtonPanel.h"

using namespace std;

ButtonPanel::ButtonPanel(WINDOW* iWindow, const int* iLastKey){
	window = iWindow;
	lastKey = NULL;
}

ButtonPanel::~ButtonPanel(){
	//for(map<const string, Button*>::iterator it = buttons.begin(); it != buttons.end(); ++it){ delete it->second; }
	for(map<const int, Button*>::iterator it = buttons.begin(); it != buttons.end(); ++it){ delete it->second; }
	buttons.clear();

}

void ButtonPanel::Add(const string iName, const unsigned int iRow, const unsigned int iCol, const string iText, const int iKey){
	buttons[iKey] = new Button(window, iRow, iCol, iText, iKey);
}

