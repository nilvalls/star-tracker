#include "Controls.h"

using namespace std;

Controls::Controls(WINDOW* iWindow, int* iLastKey) : ButtonPanel(iWindow, iLastKey){

	window = iWindow;
	lastKey = iLastKey;

	wattron(window, COLOR_PAIR(3));
	Add("ra_mm",		4, 0,	" -- [R] ",	114);
	Add("ra_m",			4, 11,	"  - [T] ",	116);
	Add("ra_stop",		4, 22,	"  [Y]  ",	121);
	Add("ra_p",			4, 32,	" [U] +  ",	117);
	Add("ra_pp",		4, 43,	" [I] ++ ", 105);

	Add("dec_mm",		6, 0,	" -- [F] ",	102);
	Add("dec_m",		6, 11,	"  - [G] ",	103);
	Add("dec_stop",		6, 22,	"  [H]  ",	104);
	Add("dec_p",		6, 32,	" [J] +  ",	106);
	Add("dec_pp",		6, 43,	" [K] ++ ",	107);

	Add("foc_mm",		8, 0,	" -- [C] ",	99);
	Add("foc_m",		8, 11,	"  - [V] ",	118);
	Add("foc_stop",		8, 22,	"  [B]  ",	98);
	Add("foc_p",		8, 32,	" [N] +  ",	110);
	Add("foc_pp",		8, 43,	" [M] ++ ",	109);
	wattroff(window, COLOR_PAIR(3));

	wrefresh(window);

}

Controls::~Controls(){

}

void Controls::Update(const int* iKey){

	int key = *iKey;

	for(map<int, Button*>::iterator it = buttons.begin(); it != buttons.end(); ++it){
		it->second->Update();
	}

	wrefresh(window);
}
