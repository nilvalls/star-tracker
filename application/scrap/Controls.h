#ifndef Controls_H
#define Controls_H

#include "ButtonPanel.h"

class Controls : public ButtonPanel{

	// === Functions and Operators === //
	public:
		Controls(WINDOW*, int*);
		~Controls();

		void Update(const int*);

	protected:

	private:


	// === Variables === //
	public:

	protected:

	private:
};


#endif
